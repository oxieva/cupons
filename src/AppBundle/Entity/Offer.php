<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * Offer
 *
 * @ORM\Table(name="offer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferRepository")
 */
class Offer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $slug;


    /**
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 30)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="conditions", type="text", length=255)
     */
    private $conditions;

    /**
     * @Assert\Image(maxSize = "500k")
     */
    protected $foto;

    /**
     * @var string
     *
     * @ORM\Column(name="imageRoute", type="string", length=255)
     */
    private $imageRoute;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", scale=2)
     * @Assert\Range(min = 0)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="discount",type="decimal", scale=2))
     */
    private $discount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datePublicated", type="datetime", nullable=true)
     */
    private $datePublicated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExpirated", type="datetime", nullable=true)
     */
    private $dateExpirated;

    /**
     * @var integer
     *
     * @ORM\Column(name="sales", type="integer")
     */
    private $sales;

    /**
     * @var integer
     *
     * @ORM\Column(name="threshold", type="integer")
     * @Assert\Range(min = 0)
     */
    private $threshold;

    /**
     * @var bool
     *
     * @ORM\Column(name="revised", type="boolean")
     */
    private $revised;

    /**
     *
     * @ORM\ManyToOne(targetEntity="City")
     */
    private $city;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     */
    private $shop;

    /**
     * @Assert\IsTrue(message = "La date de expiración debe ser posterior a
     * la date de publicación")
     */
    public function isFechaValida()
    {
        if ($this->datePublicated == null
            ||
            $this->dateExpirated == null) {
            return true;
        }
        return $this->dateExpirated > $this->datePublicated;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param string $conditions
     */
    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * @return UploadedFile
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param UploadedFile $foto
     */
    public function setFoto(UploadedFile $foto = null)
    {
        $this->foto = $foto;
    }



    /**
     * @return string
     */
    public function getImageRoute()
    {
        return $this->imageRoute;
    }

    /**
     * @param string $imageRoute
     */
    public function setImageRoute($imageRoute)
    {
        $this->imageRoute = $imageRoute;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param string $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return \DateTime
     */
    public function getDatePublicated()
    {
        return $this->datePublicated;
    }

    /**
     * @param \DateTime $datePublicated
     */
    public function setDatePublicated($datePublicated)
    {
        $this->datePublicated = $datePublicated;
    }

    /**
     * @return \DateTime
     */
    public function getDateExpirated()
    {
        return $this->dateExpirated;
    }

    /**
     * @param \DateTime $dateExpirated
     */
    public function setDateExpirated($dateExpirated)
    {
        $this->dateExpirated = $dateExpirated;
    }

    /**
     * @return int
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param int $sales
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return int
     */
    public function getThreshold()
    {
        return $this->threshold;
    }

    /**
     * @param int $threshold
     */
    public function setThreshold($threshold)
    {
        $this->threshold = $threshold;
    }

    /**
     * @return bool
     */
    public function isRevised()
    {
        return $this->revised;
    }

    /**
     * @param bool $revised
     */
    public function setRevised($revised)
    {
        $this->revised = $revised;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function subirFoto($directorioDestino)
    {
        if (null === $this->foto) {
            return;
        }
        $directorioDestino = __DIR__ . '/../../../web/bundles/app/uploads/images';
        //dump($directorioDestino); exit();

        $nombreArchivoFoto = $this->foto->getClientOriginalName();

        $this->foto->move($directorioDestino, $nombreArchivoFoto);
        $this->setImageRoute($nombreArchivoFoto);

    }
}


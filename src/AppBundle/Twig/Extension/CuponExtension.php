<?php

namespace AppBundle\Twig\Extension;

class CuponExtension extends \Twig_Extension
{

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('discount', array($this, 'discount')),
        );
    }

    public function discount($price, $discount, $decimales = 0)
    {
        if (!is_numeric($price) || !is_numeric($discount)) {
            return '-';
        }
        if ($discount == 0 || $discount == null) {
            return '0%';
        }
        $price_original = $price + $discount;
        $porcentaje = ($discount / $price_original) * 100;
        return '-'.number_format($porcentaje, $decimales).'%';
    }

    public function getName()
    {
        return 'cupon';
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter(
                'mostrar_como_lista',
                array($this, 'mostrarComoLista'),
                array('is_safe' => array('html'))
            ),

            new \Twig_SimpleFilter(
                'cuenta_atras',
                array($this, 'cuentaAtras'),
                array('is_safe' => array('html'))
            ),
        );
    }

    public function mostrarComoLista($value, $tipo='ol')
    {
        $html = "<".$tipo.">\n";
        $html .= "<li>".str_replace("\n","</li>\n<li>", $value)."</li>\n";
        $html .= "</".$tipo.">";
        return $html;
    }

    /**
     * Transforma una fecha en una cuenta atrás actualizada en tiempo
     * real mediante JavaScript.
     *
     * La cuenta atrás se muestra en un elemento HTML con un atributo
     * `id` generado automáticamente, para que se puedan añadir varias
     * cuentas atrás en la misma página.
     *
     * @param \DateTime $fecha Objeto que representa la fecha original
     *
     * @return string
     */
    public function cuentaAtras(\DateTime $fecha)
    {
        // En JavaScript los meses empiezan a contar en 0 y acaban en 12
        // En PHP los meses van de 1 a 12, por lo que hay que convertir la fecha

        $fecha = $fecha->format('Y,')
            .($fecha->format('m')-1)
            .$fecha->format(',d,H,i,s');
        $html = <<<EOJ
    <script type="text/javascript">
    function muestraCuentaAtras(){
    var horas, minutos, segundos;
    var ahora = new Date();
    var fechaExpiracion = new Date($fecha);
    var falta = Math.floor( (fechaExpiracion.getTime() - ahora.getTime()) / 1000 );
    if (falta < 0) {
        cuentaAtras = '-';
    } else {
        horas = Math.floor(falta/3600);
        falta = falta % 3600;
        minutos = Math.floor(falta/60);
        falta = falta % 60;
        segundos = Math.floor(falta);
        cuentaAtras = (horas < 10? '0' + horas: horas) + 'h '+ (minutos < 10 ? '0' + minutos : minutos) + 'm '
            + (segundos < 10 ? '0' + segundos : segundos) + 's ';
        
        setTimeout('muestraCuentaAtras()', 1000);
    }
        document.getElementById('tiempo').innerHTML = '<strong>Falten:</strong> ' + cuentaAtras;
    }
        muestraCuentaAtras();
    </script>
EOJ;
        return $html;
    }
    /*$fechaJson = json_encode(array(
        'ano' => $fecha->format('Y'),
        'mes' => $fecha->format('m') - 1,
        'dia' => $fecha->format('d'),
        'hora' => $fecha->format('H'),
        'minuto' => $fecha->format('i'),
        'segundo' => $fecha->format('s'),
    ));
    $idAleatorio = 'cuenta-atras-'.mt_rand(1, 100000);
    $html = <<<EOJ
    <span id="$idAleatorio"></span>
    <script type="text/javascript">
    funcion_expira = function(){
        var expira = $fechaJson;
        muestraCuentaAtras('$idAleatorio', expira);
    }
    if (!window.addEventListener) {
        window.attachEvent("onload", funcion_expira);
    } else {
        window.addEventListener('load', funcion_expira);
    }
    </script>
EOJ;
    return $html;*/



}
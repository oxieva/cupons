<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;

class EmailOfertaDelDiaCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:email:offer-del-dia')
            ->setDefinition(array(
                new InputArgument(
                    'city',
                    InputArgument::OPTIONAL,
                    'El slug de la city para la que se generan los emails'
                ),
                new InputOption(
                    'accion',
                    null,
                    InputOption::VALUE_OPTIONAL,
                    'Indica si los emails sólo se generan o también se envían',
                    'enviar'
                ),
            ))
            ->setHelp('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $accion = $input->getOption('accion');
        $city = $input->getArgument('city');

        $contenedor = $this->getContainer();
        
        $em = $this->getContainer()->get('doctrine')->getManager();
// Obtener el listado de usuarios que permiten el envío de email
        $users = $em->getRepository('AppBundle:User')
            ->findBy(array('allowEmail' => true));

        $io->comment(sprintf(
            'Se van a enviar <info>%s</info> emails', count($users)
        ));
// Buscar la 'offer del día' en todas las cityes de la aplicación
        $offers = array();
        $cities = $em->getRepository('AppBundle:City')->findAll();
        foreach ($cities as $city) {
            $id = $city->getId();
            $slug = $city->getSlug();
            $offers[$id] = $em->getRepository('AppBundle:Offer')
                ->findOfferNextDay($slug);
        }


        // Generar el email personalizado de cada usuario
        foreach ($users as $user) {
            $city = $user->getCity();
            $offer = $offers[$city->getId()];
            $contenido = $contenedor->get('twig')->render(
                'AppBundle:email:offerDay.html.twig', array(
                    'city' => $city,
                    'offer' => $offer,
                    'user' => $user)
            );
// Enviar el email ...
        }

        $mensaje = \Swift_Message::newInstance()
            ->setSubject('Oferta del día')
            ->setFrom('mailing@cupon.com')
            ->setTo('eva.lujan@gmail.com')
            ->setBody($contenido, 'text/html')
        ;
        $contenedor->get('mailer')->send($mensaje);
    }
}
<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\IsTrue;

class OfferType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class)
                ->add('slug', TextType::class)
                ->add('description',TextareaType::class)
                ->add('conditions',TextType::class)
                ->add('foto',FileType::class, array('required' => false))
                ->add('price', MoneyType::class)
                ->add('discount', MoneyType::class)
                ->add('threshold')
                ->add('revised')
                ->add('acepto', CheckboxType::class, array(
                    'mapped' => false,
                    'constraints' => new IsTrue(array('message' => 'Debes aceptar las condiciones legales antes de añadir
                     una oferta',
                ))
            ))
                ->add('guardar', SubmitType::class, ['label' => 'Create Offer']);

        if (true === $options['mostrar_condiciones']) {
            // Cuando se crea una oferta, se muestra un checkbox para aceptar las
            // condiciones de uso
            $builder->add('acepto', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array(
                'mapped' => false,
                'constraints' => new IsTrue(array(
                    'message' => 'Debes aceptar las condiciones indicadas antes de poder añadir una nueva oferta',
                )),
            ));
        }

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Offer',
            'mostrar_condiciones' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'offer';
    }


}

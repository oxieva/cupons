<?php
namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('email', EmailType::class)

            ->add('direction', TextType::class)
            ->add('allowEmail', CheckboxType::class, array('required' => false))
            ->add('dateBirth', BirthdayType::class)
            ->add('dni', TextType::class)
            ->add('cardNumber',TextType::class)
            ->add('city')
        ;

        if ('create_user' === $options['accion']) {
            $builder->add('register', SubmitType::class, array(
                'label' => 'Register me',
            ));

            $builder->add('passwordClearly', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Las dos contraseñas deben coincidir',
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ));

        } elseif ('modify_profile' === $options['accion']) {
            $builder->add('save', SubmitType::class, array(
                'label' => 'Save changes',
            ));
            $builder->add('passwordClearly', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Las dos contraseñas deben coincidir',
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
                'required' => false
            ));
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'accion' =>'modify_profile'
        ));
    }

    public function getBlockPrefix()
    {
        return 'user';
    }
}
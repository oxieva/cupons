<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 17/01/19
 * Time: 10:43
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Users implements FixtureInterface, ContainerAwareInterface
{
    private $container;
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=500; $i++) {
            $user = new User();
            $encoder = $this->container->get('security.encoder_factory')
                ->getEncoder($user);
            $passwordEnClaro = 'user'.$i;
            $password = $encoder->encodePassword($passwordEnClaro, null);
            $user->setPassword($password);
        }
    }
}
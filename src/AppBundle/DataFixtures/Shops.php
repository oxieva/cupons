<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 21/01/19
 * Time: 10:18
 */

namespace AppBundle\DataFixtures;

use AppBundle\Entity\Shop;
use AppBundle\Entity\City;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Shops extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 20;
    }

    public function load(ObjectManager $manager)
    {
        // Obtener todas las ciudades de la base de datos
        $cities = $manager->getRepository('AppBundle:City')->findAll();

        foreach ($cities as $i =>$city) {
            for ($j=1; $j<=rand(2, 5); $j++) {
                $shop = new Shop();
                $shop->setName($this->getName());
                $shop->setLogin('shop'.$i);

                $encoder = $this->container->get('security.encoder_factory')
                    ->getEncoder($shop);
                $passwordEnClaro = 'shop'.$i;
                $passwordCodificado = $encoder->encodePassword(
                    $passwordEnClaro,
                    $shop->getSalt()
                );
                $shop->setPassword($passwordCodificado);
                $shop->setPasswordEnClaro('shop'.$i);
                $shop->setDescription($this->getDescription());
                $shop->setSlug('slug'. $i);
                $shop->setAdress($this->getDirection($city));
                $shop->setCity($city);


                $manager->persist($shop);
            }
        }
        $manager->flush();
    }

    /**
     * Generador aleatorio de nombres de shops.
     *
     * @return string
     */
    private function getName()
    {
        $prefijos = array('Restaurante', 'Cafetería', 'Bar', 'Pub', 'Pizza', 'Burger');
        $nombres = array(
            'Lorem ipsum', 'Sit amet', 'Consectetur', 'Adipiscing elit',
            'Nec sapien', 'Tincidunt', 'Facilisis', 'Nulla scelerisque',
            'Blandit ligula', 'Eget', 'Hendrerit', 'Malesuada', 'Enim sit',
        );
        return $prefijos[array_rand($prefijos)].' '.$nombres[array_rand($nombres)];
    }

    /**
     * Generador aleatorio de descripciones de shops.
     *
     * @return string
     */
    private function getDescription()
    {
        $frases = array_flip(array(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'Mauris ultricies nunc nec sapien tincidunt facilisis.',
            'Nulla scelerisque blandit ligula eget hendrerit.',
            'Sed malesuada, enim sit amet ultricies semper, elit leo lacinia massa, in tempus nisl ipsum quis libero.',
            'Aliquam molestie neque non augue molestie bibendum.',
            'Pellentesque ultricies erat ac lorem pharetra vulputate.',
            'Donec dapibus blandit odio, in auctor turpis commodo ut.',
            'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
            'Nam rhoncus lorem sed libero hendrerit accumsan.',
            'Maecenas non erat eu justo rutrum condimentum.',
            'Suspendisse leo tortor, tempus in lacinia sit amet, varius eu urna.',
            'Phasellus eu leo tellus, et accumsan libero.',
            'Pellentesque fringilla ipsum nec justo tempus elementum.',
            'Aliquam dapibus metus aliquam ante lacinia blandit.',
            'Donec ornare lacus vitae dolor imperdiet vitae ultricies nibh congue.',
        ));
        $numeroFrases = mt_rand(3, 6);
        return implode(' ', array_rand($frases, $numeroFrases));
    }

    /**
     * Generador aleatorio de direcciones postales.
     *
     * @param City $city
     *
     * @return string
     */
    private function getDirection(City $city)
    {
        $prefijos = array('Calle', 'Avenida', 'Plaza');
        $nombres = array(
            'Lorem', 'Ipsum', 'Sitamet', 'Consectetur', 'Adipiscing',
            'Necsapien', 'Tincidunt', 'Facilisis', 'Nulla', 'Scelerisque',
            'Blandit', 'Ligula', 'Eget', 'Hendrerit', 'Malesuada', 'Enimsit',
        );
        return $prefijos[array_rand($prefijos)].' '.$nombres[array_rand($nombres)].', '.mt_rand(1, 100)."\n"
            .$this->getCodigoPostal().' '.$city->getName();
    }

    /**
     * Generador aleatorio de códigos postales.
     *
     * @return string
     */
    private function getCodigoPostal()
    {
        return sprintf('%02s%03s', mt_rand(1, 52), mt_rand(0, 999));
    }
}
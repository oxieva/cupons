<?php


namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Offer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ExtranetController extends Controller
{
    /**
     * @Route("/", name="extranet_homepage")
     */
    public function homepageAction() {
        $em = $this->getDoctrine()->getManager();

        $shop = $this->get('security.token_storage')->getToken()->getUser();

        $offers = $em->getRepository('AppBundle:Shop')
                    ->findOffersRecent($shop->getId());

        //dump($offers); exit();
        return $this->render('AppBundle:extranet:homepage.html.twig', array(
            'offers' => $offers,
        ));
    }
    /**
     * @Route("/offer/sales/{id}", name="extranet_offer_sales")
     */
    public function offerSalesAction($id) {
        $em = $this->getDoctrine()->getManager();
        $sales = $em->getRepository('AppBundle:Offer')
            ->findSalesByOffer($id);
        return $this->render('AppBundle:extranet:sales.html.twig', array(
            'offer' => $sales[0]->getOffer(),
            'sales' => $sales
        ));
    }
    /**
     * @Route("/offer/new", name="extranet_offer_new")
     */
    public function offerNewAction(Request $request) {

        $offer = new Offer();

        $formulario = $this->createForm('AppBundle\Form\OfferType', $offer);

        $formulario->handleRequest($request);
        //dump($this->container->getParameter('cupon.directorio.imagenes')); exit();
        if ($formulario->isValid()) {


            $em = $this->getDoctrine()->getManager();

            $shop = $this->getUser();
            $offer->setSales(0);
            $offer->setShop($shop);
            $offer->setCity($shop->getCity());
            //$offer->setSlug('hola');
            $offer->subirFoto($this->container->getParameter('cupon.directorio.imagenes'));

            //dump($offer);
//dump($this->container->getParameter('cupon.directorio.imagenes')); exit();

            $em->persist($offer);
            $em->flush();
            return $this->redirectToRoute('extranet_homepage');
        }

        return $this->render('AppBundle:extranet:offer.html.twig', array(
            'accion' => 'crear',
            'formulario' => $formulario->createView()
        ));
    }
    /**
     * @Route("/offer/edit/{id}", name="extranet_offer_edit")
     */
    public function offerEditAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $offer = $em->getRepository('AppBundle:Offer')->find($id);
        if (!$offer) {
            throw $this->createNotFoundException('La offer no existe');
        }
        if (false === $this->isGranted('ROLE_EDIT_OFFER', $offer)) {
            throw new AccessDeniedException();
        }
        if ($offer->isRevised()) {
            $this->addFlash('error', 'La offer no se puede modificar porque ya ha sido revisada');
            return $this->redirect($this->generateUrl('extranet_homepage'));
        }
        $formulario = $this->createForm('AppBundle\Form\OfferType', $offer);
        $formulario->handleRequest($request);
        if ($formulario->isValid()) {

            /*Per no haver de tornar a pujar la imatge,si editem la offer*/
            $rutaFotoOriginal = $formulario->getData()->getImageRoute();

            //dump($rutaFotoOriginal); exit();

            if (null == $offer->getFoto()) {
                // La foto original no se modifica, recuperar su ruta
                $offer->setImageRoute($rutaFotoOriginal);
                //dump($offer); exit();

            } else {
                // La foto de la offer se ha modificado
                $directorioFotos = $this->container->getParameter(
                    'cupon.directorio.imagenes'
                );
                //dump($directorioFotos); exit();
                $offer->subirFoto($directorioFotos);

                //$fotoOriginal =$offer->getFoto();
                //dump($fotoOriginal); exit();
                // Borrar la foto anterior
                unlink($directorioFotos.$rutaFotoOriginal);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($offer);
            $em->flush();
            return $this->redirect(
                $this->generateUrl('extranet_homepage')
            );
        }
        return $this->render('AppBundle:extranet:offer.html.twig',
            array(
                'accion' => 'editar',
                'offer' => $offer,
                'formulario' => $formulario->createView()
            )
        );
    }

    /**
     * @Route("/profile", name="extranet_profile")
     */
    public function profileAction(Request $request) {

        $shop = $this->getUser();
        $formulario = $this->createForm('AppBundle\Form\ShopType', $shop);
        $formulario->handleRequest($request);
        if ($formulario->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($shop);
            $em->flush();
            $this->addFlash('info', 'Los datos de tu perfil se han actualizado chachi');

            return $this->redirectToRoute('extranet_homepage');
        }

        return $this->render('AppBundle:extranet:profile.html.twig', array(
            'shop' => $shop,
            'formulario' => $formulario->createView()
        ));
    }

    /**
     * @Route("/login", name="extranet_login")
     */
    public function loginAction()
    {
        $authUtils = $this->get('security.authentication_utils');
//dump($authUtils); exit();
        return $this->render('AppBundle:extranet:login.html.twig', array(
            'last_username' => $authUtils->getLastUsername(),
            'error' => $authUtils->getLastAuthenticationError(),
        ));
    }

    /**
     * @Route("/login_check", name="extranet_login_check")
     */
    public function loginCheckAction()
    {
        //dump('hola'); exit();
        // el "login check" lo hace Symfony automáticamente, por lo que
        // no hay que añadir ningún código en este método
    }

    /**
     * @Route("/logout", name="extranet_logout")
     */
    public function logoutAction()
    {
        // el logout lo hace Symfony automáticamente, por lo que
        // no hay que añadir ningún código en este método
    }
}
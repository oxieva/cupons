<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
class DefaultController extends Controller
{
    /**
     * @Route("/{city}",
     * defaults = { "city" = "%app.city_default%" },
     * name="homepage")
     * @Cache(smaxage="60")
     */

    public function homeAction($city, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $locale = $request->getLocale();
        /*$locale =$this->getRequest()->setLocale('es_ES');*/
        //dump($locale); exit();

        // Traducción a través de claves
        $titulo = $this->get('translator')->trans('menu.dia');
        $translated = $this->get('translator')->trans('About the shop');
        //dump($titulo); exit();

        $offer = $em->getRepository('AppBundle:Offer')->findDayOffer($city);
        if (!$offer) {
            throw $this->createNotFoundException(
                'No se ha encontrado la oferta del día en la ciudad seleccionada'
            );
        }

        /*$ciudad = $request->query->get('city');
        dump($ciudad); exit();*/

        $ciudad = $request->query->get('ciudad', 'paris');

        // Saber qué navegador utiliza el usuario mediante la cabecera HTTP_USER_AGENT
        $navegador = $request->server->get('HTTP_USER_AGENT');

        // Mismo ejemplo, pero más fácil directamente a través de las cabeceras
        $navegador = $request->headers->get('user-agent');

        // Obtener el nombre de todas las cabeceras enviadas
        $cabeceras = $request->headers->keys();

        // Saber si se ha enviado una cookie de sesión
        $hayCookieSesion = $request->cookies->has('PHPSESSID');
        /*dump($hayCookieSesion); exit();*/

        $response = $this->render('AppBundle::home.html.twig', array(
                'offer' => $offer
            )
        );
        //360s
        /*$response->setCache([
            'max_age'       => 10,
            's_maxage'      => 10,
            'public'        => true,
            // 'private'    => true,
        ]);*/

        /*$response->setMaxAge(3600);
        $response->setSharedMaxAge(3600);*/
        /*$response->setEtag(md5($response->getContent()));


        //dump($response->getEtag()); exit();
        $response->setPublic(); // make sure the response is public/cacheable
        $response->isNotModified($request);*/

        //$fecha = $offer->getUpdatedAt();
        //$response->getLastModified();
        //$response->isNotModified($request);
        //dump($response); exit();

        $response->setVary('Accept-Encoding', 'User-Agent');
        //dump($response); exit();
        // (optional) set a custom Cache-Control directive
        //$response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;



    }
    /**
     * @Route("/site/{namePage}",
     *     defaults={"namePage": "help"},
     *     requirements={ "namePage" = "help|privacity|about"},
     *     name="page")
     */
    public function pageAction($namePage)
    {
        return $this->render('AppBundle:site:' . $namePage . '.html.twig');
    }

    /**
     * @Route("/privacity")
     */
    public function privacityAction()
    {
        return $this->render('site/privacity.html.twig');
    }

    /**
     * @Route("/about")
     */
    public function aboutAction()
    {
        return $this->render('site/about.html.twig');
    }

}

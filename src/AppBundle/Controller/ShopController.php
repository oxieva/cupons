<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 16/01/19
 * Time: 09:12
 */

namespace AppBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
class ShopController extends Controller
{

    /**
     * @Route("/{city}/shops/{shop}",
     * requirements={ "city" = ".+" },
     * name="shop_home")
     */
    public function shopHomeAction(Request $request, $city, $shop)
    {
        $em = $this->getDoctrine()->getManager();

        $city = $em->getRepository('AppBundle:City')
            ->findOneBySlug($city);
        $shop = $em->getRepository('AppBundle:Shop')->findOneBy(array(
            'slug' => $shop,
            'city' => $city->getId()
        ));

        if (!$shop) {
            throw $this->createNotFoundException('No existe esta shop');
        }

        $offers = $em->getRepository('AppBundle:Shop')
            ->findLastOffersPublicated($shop->getId());

        $nearbies = $em->getRepository('AppBundle:Shop')->findNearbies(
            $shop->getSlug(),
            $shop->getCity()->getSlug()
        );

        return $this->render('AppBundle:shop:homepage.html.twig', array(
            'shop' => $shop,
            'offers' => $offers,
            'nearbies' => $nearbies
        ));
    }
}
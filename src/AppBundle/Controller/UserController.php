<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

/**
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/shopping", name="shopping_user")
     */
    public function shoppingAction()
    {
        $userId = 1;
        $em = $this->getDoctrine()->getManager();
        $shopping = $em->getRepository('AppBundle:User')
            ->findAllShopping($userId);

        //dump($authUtils); exit();

        return $this->render('AppBundle:user:shopping.html.twig', array(
            'shopping' => $shopping
        ));
    }

    /**
     * @Route("/login", name="user_login")
     */
    public function loginAction()
    {
        $authUtils = $this->get('security.authentication_utils');

        return $this->render('AppBundle:user:login.html.twig', array(
            'last_username' => $authUtils->getLastUsername(),
            'error' => $authUtils->getLastAuthenticationError(),
        ));

    }
    /**
     * @Route("/login_check", name="user_login_check")
     */
    public function loginCheckAction()
    {
// el "login check" lo hace Symfony automáticamente, por lo que
// no hay que añadir ningún código en este método
    }
    /**
     * @Route("/logout", name="user_logout")
     */
    public function logoutAction()
    {
// el logout lo hace Symfony automáticamente, por lo que
// no hay que añadir ningún código en este método
    }

    /**
     * @Cache(maxage="30")
     */
    public function loginBoxAction(){

        $user = $this->get('security.token_storage')->getToken()->getUser();

        /*return $this->render('AppBundle:user:_login_box.html.twig', array(
            'user' => $user,
        ));*/
        $authUtils = $this->get('security.authentication_utils');
        return $this->render('AppBundle:user:_login_box.html.twig', array(
            'user' => $user,
            'last_username' => $authUtils->getLastUsername(),
            'error' => $authUtils->getLastAuthenticationError(),
        ));
    }

    /**
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $user->setAllowEmail(true);

        $form = $this->createForm(UserType::class, $user, array(
            'accion' => 'create_user',
            'validation_groups' => array('default', 'register'),
        ));
        /*$form->add('register', SubmitType::class);*/


        $form->handleRequest($request);
        if ($form->isValid()) {
                // Validar los datos enviados y guardarlos en la base de datos
                if (null !== $user->getPasswordClearly()) {
                    $encoder = $this->get('security.encoder_factory')
                        ->getEncoder($user);
                    $passwordCodificado = $encoder->encodePassword(
                        $user->getPasswordClearly(),
                        null
                    );

                    $user->setPassword($passwordCodificado);
                }


                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('info', '¡Enhorabuena! Te has registrado correctament en Cupon');

                /*Per loguejar l'usuari acabat de registrar*/
                $token = new UsernamePasswordToken(
                    $user,
                    $user->getPassword(),
                    'frontend',
                    $user->getRoles()
                );
                $this->container->get('security.token_storage')->setToken($token);
                return $this->redirectToRoute('homepage', array(
                    'city' => $user->getCity()->getSlug()
                ));
            }

        return $this->render('AppBundle:user:register.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/profile", name="user_profile")
     */
    public function profileUserAction(Request $request){

        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user, array(
            'accion' => 'modify_profile'
        ));
        /*$form->add('save',SubmitType::class, array(
            'label' => 'Save changes'
        ));*/

        $form->handleRequest($request);
        if ($form->isValid()) {
// actualizar el perfil del user
            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $passwordCodificado = $encoder->encodePassword($user->getPassword(), null);
            $user->setPassword($passwordCodificado);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('info', '¡Enhorabuena! Los datos de tu perfil se han actualizado correctamente');

            /*Per loguejar l'usuari acabat de registrar*/
            $token = new UsernamePasswordToken(
                $user,
                $user->getPassword(),
                'frontend',
                $user->getRoles()
            );
            $this->container->get('security.token_storage')->setToken($token);

            return $this->redirectToRoute('user_profile');
        }

        return $this->render('AppBundle:user:profile.html.twig', array(
            'user' => $user,
            'form' => $form->createView()
        ));

    }

}
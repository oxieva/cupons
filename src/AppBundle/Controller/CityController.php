<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 14/01/19
 * Time: 18:04
 */

namespace AppBundle\Controller;

use AppBundle\Repository\CityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class CityController extends Controller
{
    /**
     * @Route("/city/change_to_{city}", name="city_change")
     */
    public function changeCityAction($city)
    {
        return $this->redirectToRoute('homepage', array('city' => $city));
    }

    public function listCitiesAction($city){
        $em = $this->getDoctrine()->getManager();
        $ciudades = $em->getRepository('AppBundle:City')->findAll();
        return $this->render('AppBundle:city:_list_cities.html.twig', array(
            'ciudadActual' => $city,
            'ciudades' => $ciudades
        ));
    }

    /**
     * @Route("/{city}/recent", name="city_recent")
     */
    public function recentAction($city)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$city) {
            throw $this->createNotFoundException('No existe la ciudad');
        }

        $city = $em->getRepository('AppBundle:City')
            ->findOneBySlug($city);

        $nearbies = $em->getRepository('AppBundle:City')
            ->findNearby($city->getId());

        $offers = $em->getRepository('AppBundle:Offer')
            ->findRecent($city->getId());

        //dump($ofertas); exit();
        return $this->render('AppBundle:city:recent.html.twig', array(
            'city' => $city,
            'nearbies' => $nearbies,
            'offers' => $offers
        ));
    }


}
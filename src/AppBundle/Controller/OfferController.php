<?php
/**
 * Created by PhpStorm.
 * User: eva
 * Date: 15/01/19
 * Time: 08:57
 */

namespace AppBundle\Controller;

use AppBundle\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
Use Symfony\Component\Routing\Annotation\Route;

class OfferController extends Controller
{
    /**
     * @Route("/{city}/offers/{slug}", name="offer")
     */
    public function offerAction($city, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $offer = $em->getRepository('AppBundle:Offer')
            ->findOffer($city, $slug);

        $relacionadas = $em->getRepository('AppBundle:Offer')
            ->findRelated($city);
        //dump($relacionadas); exit();

        if(!$offer){
            throw $this->createNotFoundException('No existe la oferta');
        }

        return $this->render('AppBundle:offer:detail.html.twig', array(
            'offer' => $offer,
            'relacionadas' => $relacionadas
        ));
    }



}
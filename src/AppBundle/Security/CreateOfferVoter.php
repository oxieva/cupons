<?php
namespace AppBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use AppBundle\Entity\Shop;
use AppBundle\Entity\Offer;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class CreateOfferVoter extends Voter
{
    protected function supports($attribute, $subject){
        return $subject instanceof Offer && 'ROLE_EDIT_OFFER' === $attribute;
    }

    /**
     * Devuelve 'true' si el usuario logueado es de tipo Tienda y es el creador
     * de la oferta que se quiere modificar.En el caso del voter que se está desarrollando, la lógica consiste en comparar
     * si el id de la tienda que solicita el permiso coincide con el id asociado a la tienda de la oferta que se quiere
     * modificar:
     *
     * @param string         $attribute
     * @param mixed          $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token){

        $shop = $token->getUser();
        return $shop instanceof Shop && $subject->getShop()->getId() === $shop->getId();
    }
}
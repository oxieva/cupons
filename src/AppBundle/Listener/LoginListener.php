<?php

namespace AppBundle\Listener;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
class LoginListener
{
    private $checker, $router, $city = null;
    public function __construct(AuthorizationChecker $checker, Router $router)
    {
        $this->checker = $checker;
        $this->router = $router;
    }
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        $this->city = $token->getUser()->getCity()->getSlug();
    }
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (null === $this->city) {
            return;
        }
        if($this->checker->isGranted('ROLE_SHOP')) {
            $urlPortada = $this->router->generate('extranet_homepage');
        }
        else {
            $urlPortada = $this->router->generate('homepage', array(
                'city' => $this->city
            ));
        }

        $event->setResponse(new RedirectResponse($urlPortada));
    }
}
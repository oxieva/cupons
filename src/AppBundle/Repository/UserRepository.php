<?php

namespace AppBundle\Repository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllShopping($user){
        $em = $this->getEntityManager();

        $consulta = $em->createQuery('
          SELECT v, o, s
          FROM AppBundle:Sale v
          JOIN v.offer o
          JOIN o.shop s
          WHERE v.user = :id
          ORDER BY v.dateSale DESC');

        $consulta->setParameter('id', $user);

        return $consulta->getResult();
    }
}

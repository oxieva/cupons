<?php
namespace Tests\AppBundle\Entity;

use Symfony\Component\Validator\Validation;
use PHPUnit\Framework\TestCase;
use AppBundle\Entity\Offer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OfferTest extends TestCase{

    private $validator;

    public function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
    }
    public function testValidateSlug()
    {
        $offer = new Offer();
        $offer->setName('offer test');
        $slug = $offer->getSlug();

        $this->assertEquals('offer-test', $slug,
            'El slug se asigna automáticamente a partir del nombre'
        );
    }

    public function testValidateDescription()
    {
        $oferta = new Offer();
        $oferta->setName('Oferta de prueba');
        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0, $listaErrores->count(),
            'La descripción no puede dejarse en blanco'
        );

        $error = $listaErrores[0];
        $this->assertEquals('This value should not be blank.', $error->getMessage());
        $this->assertEquals('descripcion', $error->getPropertyPath());
        $oferta->setDescription('Descripción de prueba');
        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0, $listaErrores->count(), 'La descripción debe tener al menos 30 caracteres');
        $error = $listaErrores[0];
        $this->assertRegExp("/This value is too short/", $error->getMessage());
        $this->assertEquals('descripcion', $error->getPropertyPath());
    }

    public function testValidateDates()
    {
        $oferta = new Offer();
        $oferta->setName('Oferta de prueba');
        $oferta->setDescription('Descripción de prueba - Descripción de prueba- Descripción de prueba');
        $oferta->setDatePublicated(new \DateTime('today'));
        $oferta->setDateExpirated(new \DateTime('yesterday'));
        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0, $listaErrores->count(),
            'La fecha de expiración debe ser posterior a la fecha de publicación'
        );
        $error = $listaErrores[0];
        $this->assertEquals('La fecha de expiración debe ser posterior a la fecha de publicación', $error->getMessage());
        $this->assertEquals('fechaValida', $error->getPropertyPath());

    }

    public function testValidatePrice()
    {
        $oferta = new Offer();
        $oferta->setName('Oferta de prueba');
        $oferta->setDescription('Descripción de prueba - Descripción de prueba - Descripción de prueba');
        $oferta->setDatePublicated(new \DateTime('today'));
        $oferta->setDateExpirated(new \DateTime('tomorrow'));
        $oferta->setThreshold(3);
        $oferta->setPrice(10);
        $listaErrores = $this->validator->validate($oferta);
        $this->assertGreaterThan(0, $listaErrores->count(),
            'El precio no puede ser un número negativo'
        );
        $error = $listaErrores[0];
        $this->assertRegExp("/This value should be .* or more/", $error->getMessageTemplate());
        $this->assertEquals('precio', $error->getPropertyPath());
    }
}

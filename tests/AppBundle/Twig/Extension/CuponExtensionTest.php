<?php

namespace Tests\AppBundle\Twig\Extension;

use PHPUnit\Framework\TestCase;
use AppBundle\Twig\Extension\CuponExtension;

class CuponExtensionTest extends TestCase
{
    public function testDiscount()
    {
        $extension = new CuponExtension();

        $this->assertEquals('-', $extension->discount(100, null),
            'El discount no puede ser null'
        );
        $this->assertEquals('-', $extension->discount('a', 3),
            'El precio debe ser un número'
        );
        $this->assertEquals('-', $extension->discount(100, 'a'),
            'El discount debe ser un número'
        );
        $this->assertEquals('0%', $extension->discount(10, 0),
            'Un discount de cero euros se muestra como 0%'
        );
        $this->assertEquals('-80%', $extension->discount(2, 8),
            'Si el precio de venta son 2 euros y el discount sobre el precio original son 8 euros, el discount es -80%'
        );
        $this->assertEquals('-33%', $extension->discount(10, 5),
            'Si el precio de venta son 10 euros y el discount sobre el precio original son 5 euros, el discount es -33%'
        );
        $this->assertEquals('-33.33%', $extension->discount(10, 5, 2),
            'Si el precio de venta son 10 euros y el discount sobre el precio original son 5 euros, el discount es 
            -33.33% con dos decimales'
        );
    }

    public function testMostrarComoLista()
    {
        $fixtures = __DIR__.'/fixtures/lista';
        $extension = new CuponExtension();
        $original = file_get_contents($fixtures.'/original.txt');
        $this->assertEquals(
            file_get_contents($fixtures.'/esperado-ul.txt'),
            $extension->mostrarComoLista($original, 'ul')
        );
        $this->assertEquals(
            file_get_contents($fixtures.'/esperado-ol.txt'),
            $extension->mostrarComoLista($original, 'ol')
        );
    }


}